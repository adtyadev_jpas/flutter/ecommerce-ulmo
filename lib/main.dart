import 'package:flutter/material.dart';
import 'package:flutter_exercise_ulmo/pages/bag_page.dart';
import 'package:flutter_exercise_ulmo/pages/catalog_page.dart';
import 'package:flutter_exercise_ulmo/pages/checkout_contact_info.dart';
import 'package:flutter_exercise_ulmo/pages/checkout_delivery_details.dart';
import 'package:flutter_exercise_ulmo/pages/checkout_payment_method.dart';
import 'package:flutter_exercise_ulmo/pages/checkout_success.dart';
import 'package:flutter_exercise_ulmo/pages/empty_bag_page.dart';
import 'package:flutter_exercise_ulmo/pages/filter_options_page.dart';
import 'package:flutter_exercise_ulmo/pages/main_page.dart';
import 'package:flutter_exercise_ulmo/pages/new_review_page.dart';
import 'package:flutter_exercise_ulmo/pages/no_intenernet_page.dart';
import 'package:flutter_exercise_ulmo/pages/product_page.dart';
import 'package:flutter_exercise_ulmo/pages/review_page.dart';
import 'package:flutter_exercise_ulmo/pages/filter_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.amber,
        fontFamily: 'Poppins',
      ),
      initialRoute: "/checkout_delivery_details",
      routes: {
        "/": (context) => const MainPage(),
        "/catalog": (context) => CatalogPage(),
        "/product": (context) => ProductPage(),
        "/review": (context) => ReviewPage(),
        "/new_review": (context) => NewReviewPage(),
        "/filter": (context) => FilterPage(),
        "/filter_options": (context) => FilterOptionsPage(),
        "/no_internet": (context) => NoInternetPage(),
        "/empty_bag" : (context) => EmptyBagPage(),
        "/bag" : (context) => BagPage(),
        "/checkout_contact_info" : (context) => CheckoutContactInfo(),
        "/checkout_delivery_details" : (context) => CheckoutDeliveryDetails(),
        "/checkout_payment_method" : (context) => CheckoutPaymentMethod(),
        "/checkout_success" : (context) => CheckoutSuccess()
      },
    );
  }
}
