List<Map<String, String>> dataCategory = [
  {
    "url": "assets/images/main1.png",
    "title": "Best of",
    "subtitle": "2020",
  },
  {
    "url": "assets/images/main2.png",
    "title": "The golden",
    "subtitle": "hour",
  },
  {
    "url": "assets/images/main3.png",
    "title": "Lovely",
    "subtitle": "kithcen",
  },
  {
    "url": "assets/images/main4.png",
    "title": "summer",
    "subtitle": "choice",
  }
];

List<Map<String, String>> dataProduk = [
  {
    "url": "assets/images/main1-1.png",
    "title": "Bedroom",
  },
  {
    "url": "assets/images/main1-2.png",
    "title": "Living Room",
  },
  {
    "url": "assets/images/main1-3.png",
    "title": "Kitchen",
  },
  {
    "url": "assets/images/main1-5.png",
    "title": "Plate",
  }
];

List<Map<String, String>> dataCatalog = [
  {
    "url": "assets/images/catalog1.png",
    "bedge": "New",
    "price": "Rp 150.000",
    "favorite": "yes",
    "description": "Wooden bedside table featuring a raised desi..."
  },
  {
    "url": "assets/images/catalog2.png",
    "bedge": "",
    "price": "Rp 120.000",
    "favorite": "no",
    "description": "Chair made of ash wood sourced from responsib..."
  },
  {
    "url": "assets/images/catalog3.png",
    "bedge": "",
    "price": "Rp 220.000",
    "favorite": "yes",
    "description": "Wooden bedside table featuring a raised design"
  },
  {
    "url": "assets/images/catalog4.png",
    "bedge": "-40%",
    "price": "Rp 250.000",
    "favorite": "no",
    "description": "Wooden bedside table featuring a raised design"
  }
];

List<Map<String, dynamic>> dataReview = [
  {
    "star": 4,
    "name": "Erin Mango",
    "time": "Today, 12:30 pm",
    "comment": "Everything is good. Nice quality",
    "attachment": true
  },
  {
    "star": 1,
    "name": "Corey Rosser",
    "time": "1 month ago",
    "comment": "Could be better :(",
    "attachment": false
  },
  {
    "star": 2,
    "name": "Paityn Saris",
    "time": "2 months ago",
    "comment":
        "Bought this table 2 months ago and I wanted to say, this is the best bedside table I’ve ever used 😍",
    "attachment": false
  },
  {
    "star": 3,
    "name": "Paityn Saris",
    "time": "2 months ago",
    "comment":
        "Bought this table 2 months ago and I wanted to say, this is the best bedside table I’ve ever used 😍",
    "attachment": false
  }
];

List<Map<String, dynamic>> dataItemsBag = [
  {
    "url" : "assets/images/bag_item1.png",
   "price" : "Rp 150.000.000",
    "description" : "Wooden bedside table featuring a raised design"
  },
  {
    "url" : "assets/images/bag_item2.png",
    "price" : "Rp 110.000.000",
    "description" : "Square bedside table with legs, a rattan shelf and a..."
  },
  {
    "url" : "assets/images/bag_item1.png",
    "price" : "Rp 130.000.000",
    "description" : "Wooden bedside table featuring a raised design"
  }
];

List ListDaysCheckout = ["Tommorrow", "Jun 25", "Jun 26", "Jun 27", "Jun 28"];
List ListTimeCheckout = ["02:00 pm", "04:00 pm", "06:00 pm", "08:00 pm", "10:00 pm", "12:00 pm"];

