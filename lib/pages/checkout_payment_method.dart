import 'package:flutter/material.dart';

class CheckoutPaymentMethod extends StatelessWidget {
  const CheckoutPaymentMethod({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Column(
          children: [
            Text(
              "Checkout",
              style: TextStyle(fontSize: 18),
            ),
            Text(
              "3 of 3",
              style: TextStyle(fontSize: 12, color: Colors.grey),
            )
          ],
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Column(
                children: [
                  Text(
                    "Payment Method",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                  ),
                  ListTile(
                    leading: Icon(Icons.payment_rounded),
                    title: Text("Mastercard 9833"),
                    subtitle: Text("734, Exp: 12/29"),
                    trailing: Icon(
                      Icons.check_circle,
                      color: Colors.amberAccent,
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.payments),
                    title: Text("Visa 7233"),
                    subtitle: Text("321, Exp: 11/29"),
                    trailing: Icon(
                      Icons.circle_outlined,
                      color: Colors.amberAccent,
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.payments),
                    title: Text("Apple Pay"),
                    trailing: Icon(
                      Icons.circle_outlined,
                      color: Colors.amberAccent,
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: Colors.amber[300],
                    minimumSize: const Size.fromHeight(50)),
                child: const Text('Pay \$420,50'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
