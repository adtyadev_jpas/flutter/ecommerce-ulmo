import 'package:flutter/material.dart';

class ProductPage extends StatelessWidget {
  const ProductPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.favorite))],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              constraints: BoxConstraints(
                maxHeight: 400,
                minHeight: 300,
              ),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/product1.png"))),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Rp 150.000.000",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                  Text(
                    "Wooden bedside table featuring a raised design on the door",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20.0),
                    child: Row(
                      children: [
                        Chip(
                          avatar: Container(
                            padding: EdgeInsets.all(1),
                            decoration: BoxDecoration(
                                color: Colors.white, shape: BoxShape.circle),
                            child: CircleAvatar(
                              backgroundColor: Colors.brown[500],
                            ),
                          ),
                          label: const Text(
                            'Brown',
                            style: TextStyle(color: Colors.white),
                          ),
                          backgroundColor: Colors.black,
                          padding: EdgeInsets.all(10),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Chip(
                          avatar: Container(
                            padding: EdgeInsets.all(1),
                            decoration: BoxDecoration(
                                color: Colors.white, shape: BoxShape.circle),
                            child: CircleAvatar(
                              backgroundColor: Colors.black,
                            ),
                          ),
                          label: const Text(
                            'Brown',
                            style: TextStyle(color: Colors.black),
                          ),
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.all(10),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey),
                              borderRadius: BorderRadius.circular(8)),
                        )
                      ],
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        primary: Colors.amber[300],
                        minimumSize: const Size.fromHeight(60)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Add to bag'), // <-- Text
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          // <-- Icon
                          Icons.shopping_bag_outlined,
                          size: 24.0,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Card(
                      elevation: 0,
                      color: Colors.grey[200],
                      child: ListTile(
                        leading: SizedBox(
                            height: double.infinity,
                            child: Icon(
                              Icons.percent_outlined,
                              color: Colors.black,
                            )),
                        title: Text("Discount for you"),
                        subtitle: Text("Use promocode ULMO"),
                        trailing: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            primary: Colors.amber[300],
                          ),
                          child: Text("Copy"),
                        ),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("Product information"),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    title: Text("Reviews"),
                    trailing: Text("32"),
                  ),
                  ListTile(
                    title: Text("Questions and answers"),
                    trailing: Text("5"),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
