import 'package:flutter/material.dart';

class NewReviewPage extends StatelessWidget {
  const NewReviewPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {},
        ),
        title: Text("New Review"),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.amberAccent[200],
                        size: 45,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.amberAccent[200],
                        size: 45,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.amberAccent[200],
                        size: 45,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.amberAccent[200],
                        size: 45,
                      ),
                      Icon(
                        Icons.star_border_outlined,
                        color: Colors.amberAccent[200],
                        size: 45,
                      )
                    ],
                  ),
                  Text(
                    "Nice",
                    style: TextStyle(color: Colors.grey, fontSize: 20),
                  ),
                ],
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 20),
              color: Colors.grey[100],
              child: ListTile(
                title: Text(
                  "Your Review",
                  style: TextStyle(fontSize: 15, color: Colors.grey),
                ),
                subtitle: Text(
                  "Everything is good. Nice quality",
                  style: TextStyle(color: Colors.black, fontSize: 18),
                ),
              ),
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.amber[300],
                    minimumSize: const Size.fromHeight(60)),
                onPressed: () {},
                child: Text("Send Review"))
          ],
        ),
      ),
    );
  }
}
