import 'package:flutter/material.dart';
import 'package:flutter_exercise_ulmo/config/constants.dart';
import 'package:flutter_exercise_ulmo/widgets/search_widget.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Text(
                "Ulmo",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 10,
              ),
              SearchWidget(),
              SizedBox(
                height: 10,
              ),
              _cardCategory(dataCategory),
              for (int i = 0; i < dataProduk.length; i++)
                _cardProduk(dataProduk[i]['url'], dataProduk[i]['title']),
            ],
          ),
        ),
      ),
    );
  }

  Widget _cardCategory(List<Map<String, String>> data) {
    return Container(
      constraints: BoxConstraints(
        maxHeight: 100,
      ),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.all(5),
              width: 100,
              foregroundDecoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black.withOpacity(0.4),
                    Colors.black.withOpacity(0),
                    Colors.black.withOpacity(0),
                    Colors.black.withOpacity(0),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage("${data[index]['url']}"),
                    fit: BoxFit.cover,
                  )),
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "${data[index]['title']} \n ${data[index]['subtitle']}",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: data.length),
    );
  }

  Widget _cardProduk(url, title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          color: Colors.grey[300],
          constraints: BoxConstraints(maxHeight: 120),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  title,
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                ),
              ),
              Image.asset(url)
            ],
          ),
        ),
      ),
    );
  }
}
