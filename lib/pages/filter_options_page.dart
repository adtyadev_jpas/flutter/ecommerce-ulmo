import 'package:flutter/material.dart';

class FilterOptionsPage extends StatefulWidget {
  const FilterOptionsPage({Key? key}) : super(key: key);

  @override
  State<FilterOptionsPage> createState() => _FilterOptionsPageState();
}

class _FilterOptionsPageState extends State<FilterOptionsPage> {
  bool isFurnitureChecked = false;
  bool isLightingChecked = false;
  bool isRugsChecked = false;
  bool isMirrorsChecked = false;
  bool isBlanketsChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text("Category"),
        centerTitle: true,
        elevation: 0,
        actions: [
          TextButton(
              onPressed: () {},
              child: Text(
                "Clear",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ))
        ],
      ),
      body: SafeArea(
          child: Stack(
        children: [
          Column(
            children: [
              ListTile(
                title: Text("Furniture"),
                trailing: Checkbox(
                  checkColor: Colors.white,
                  //fillColor: MaterialStateProperty.resolveWith(getColor),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  value: isFurnitureChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isFurnitureChecked = value!;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text("Lighting"),
                trailing: Checkbox(
                  checkColor: Colors.white,
                  //fillColor: MaterialStateProperty.resolveWith(getColor),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  value: isLightingChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isLightingChecked = value!;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text("Rugs"),
                trailing: Checkbox(
                  checkColor: Colors.white,
                  //fillColor: MaterialStateProperty.resolveWith(getColor),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  value: isRugsChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isRugsChecked = value!;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text("Mirrors"),
                trailing: Checkbox(
                  checkColor: Colors.white,
                  //fillColor: MaterialStateProperty.resolveWith(getColor),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  value: isMirrorsChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isMirrorsChecked = value!;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text("Blankets"),
                trailing: Checkbox(
                  checkColor: Colors.white,
                  //fillColor: MaterialStateProperty.resolveWith(getColor),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  value: isBlanketsChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isBlanketsChecked = value!;
                    });
                  },
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: Colors.amber[300],
                    minimumSize: const Size.fromHeight(50)),
                child: const Text('Show 25 items'),
              ),
            ),
          )
        ],
      )),
    );
  }
}
