import 'package:flutter/material.dart';
import 'package:flutter_exercise_ulmo/config/constants.dart';

class CheckoutDeliveryDetails extends StatelessWidget {
  const CheckoutDeliveryDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Column(
          children: [
            Text(
              "Checkout",
              style: TextStyle(fontSize: 18),
            ),
            Text(
              "2 of 3",
              style: TextStyle(fontSize: 12, color: Colors.grey),
            )
          ],
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Delivery Method",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            Container(
              child: Column(
                children: [
                  ListTile(
                    leading: Container(
                      height: double.infinity,
                      child: Icon(Icons.car_repair),
                    ),
                    title: Text("By courier"),
                    subtitle: Text("Tomorrow, any time"),
                    trailing: Icon(
                      Icons.check_circle,
                      color: Colors.amberAccent,
                    ),
                  ),
                  ListTile(
                    leading: Container(
                      height: double.infinity,
                      child: Icon(Icons.shopping_bag),
                    ),
                    title: Text("I'll take it myself"),
                    subtitle: Text("Any day from tomorrow"),
                    trailing: Icon(
                      Icons.circle_outlined,
                    ),
                  ),
                ],
              ),
            ),
            Text(
              "Delivery Address",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            ListTile(
              leading: Container(
                height: double.infinity,
                child: Icon(Icons.pin_drop_sharp),
              ),
              title: Text("London, 221B Baker Street"),
              subtitle: Text("Hanna Gouse, +7 932 123-43-23"),
              trailing: InkWell(
                onTap: (){
                  showModalBottomSheet<void>(
                    context: context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight:  Radius.circular(15)),
                    ),
                    backgroundColor: Colors.white,
                    builder: (BuildContext context) {
                      return Container(
                        margin: EdgeInsets.all(20),
                        height: MediaQuery.of(context).size.height/2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Delivery Address",
                              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                            ),
                            ListTile(
                              leading: Container(
                                height: double.infinity,
                                child: Icon(Icons.pin_drop),
                              ),
                              title: Text("London, 221B Baker Street"),
                              subtitle: Text("Hanna Gouse, +7 932 123-43-23"),
                              trailing: Icon(
                                Icons.check_circle,
                                color: Colors.amberAccent,
                              ),
                            ),
                            ListTile(
                              leading: Container(
                                height: double.infinity,
                                child: Icon(Icons.pin_drop),
                              ),
                              title: Text("Paris, 32B Trina Street"),
                              subtitle: Text("Randy Gouse, +12 942 422-41-32"),
                              trailing: Icon(
                                Icons.circle_outlined
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 20),
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                    elevation: 0,
                                    primary: Colors.grey[300],
                                    minimumSize: const Size.fromHeight(50)),
                                child: const Text('Cancel'),
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  );
                },
                child: Icon(
                  Icons.arrow_forward_ios,
                ),
              ),
            ),
            Text(
              "Delivery Time",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            Container(
              height: 50,
              // color: Colors.red,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: ListDaysCheckout.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.transparent),
                              elevation: MaterialStateProperty.all(0),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(color: Colors.grey)),
                              )),
                          child: Text(ListDaysCheckout[index])),
                    );
                  }),
            ),
            Container(
              height: 50,
              // color: Colors.red,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: ListTimeCheckout.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.transparent),
                              elevation: MaterialStateProperty.all(0),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(color: Colors.grey)),
                              )),
                          child: Text(ListTimeCheckout[index])),
                    );
                  }),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: Colors.amber[300],
                    minimumSize: const Size.fromHeight(50)),
                child: const Text('Continue'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
