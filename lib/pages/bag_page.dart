import 'package:flutter/material.dart';
import 'package:flutter_exercise_ulmo/config/constants.dart';
import 'package:flutter_exercise_ulmo/widgets/item_bag_widget.dart';

class BagPage extends StatefulWidget {
  const BagPage({Key? key}) : super(key: key);

  @override
  State<BagPage> createState() => _BagPageState();
}

class _BagPageState extends State<BagPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Bag",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              for (int i = 0 ; i < dataItemsBag.length ; i++)
              ItemBagWidget(
                  price: dataItemsBag[i]['price'],
                  description: dataItemsBag[i]['description'],
                  url: dataItemsBag[i]['url']),
              Text("Promocode",style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),),
              Card(
                color: Colors.grey[200],
                elevation: 0,
                child: ListTile(
                  title: Text("ULMOCODE"),
                  trailing: Icon(Icons.close_rounded),
                ),
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Total",style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)),
                  Text("Rp 420.000.000",style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Promocode",style: TextStyle(fontSize: 15, color: Colors.grey)),
                  Text("- Rp 400.000",style: TextStyle(fontSize: 15, color: Colors.grey))
                ],
              ),
              SizedBox(height: 25,),
              ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                      primary: Colors.amber[300],
                      minimumSize: const Size.fromHeight(60)),
                  child: Text("Checkout"))
            ],
          ),
        ),
      ),
    );
  }
}
