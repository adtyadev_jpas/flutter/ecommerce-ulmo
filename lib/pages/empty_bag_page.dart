import 'package:flutter/material.dart';

class EmptyBagPage extends StatelessWidget {
  const EmptyBagPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Bag",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.asset("assets/images/shape_shock.png"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("your bag is empty",
                      style:
                      TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "items remain in your bag for 1 hour, and then they’re moved to your Saved items",
                    style: TextStyle(color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
            ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    primary: Colors.amber[300],
                    minimumSize: const Size.fromHeight(60)),
                child: Text("Start Shopping"))
          ],
        ),
      ),
    );
  }
}
