import 'package:flutter/material.dart';
import 'package:flutter_exercise_ulmo/widgets/search_widget.dart';
import 'package:flutter_exercise_ulmo/config/constants.dart';

class CatalogPage extends StatelessWidget {
  const CatalogPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text("Furniture"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              const SearchWidget(),
              const SizedBox(
                height: 10,
              ),
              Container(
                constraints: const BoxConstraints(minHeight: 32, maxHeight: 52),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 8),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                              primary: Colors.grey[200], elevation: 0),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: const TextSpan(children: [
                              TextSpan(
                                  text: "Sort",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18),
                                  children: [
                                    WidgetSpan(
                                        child: Icon(Icons.sort_rounded),
                                        alignment: PlaceholderAlignment.middle)
                                  ])
                            ]),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                              primary: Colors.grey[200], elevation: 0),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: const TextSpan(children: [
                              TextSpan(
                                text: "Filter",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18),
                                children: [
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.filter_list_alt,
                                      size: 20,
                                    ),
                                  )
                                ],
                              )
                            ]),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 0.5,
                    crossAxisSpacing: 10.0,
                  ),
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                              constraints: BoxConstraints(
                                  minHeight: 150, maxHeight: 250),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "${dataCatalog[index]['url']}"),
                                      fit: BoxFit.cover)),
                            ),
                            (dataCatalog[index]['bedge'] != "")
                                ? Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Chip(
                                      label: Text(
                                          "${dataCatalog[index]['bedge']}"),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      backgroundColor: Colors.amber[300],
                                    ),
                                  )
                                : SizedBox(),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("${dataCatalog[index]['price']}"),
                            (dataCatalog[index]['favorite'] == "yes")
                                ? Icon(Icons.favorite)
                                : Icon(Icons.favorite_border)
                          ],
                        ),
                        Text("${dataCatalog[index]['description']}")
                      ],
                    );
                  },
                  itemCount: dataCatalog.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
