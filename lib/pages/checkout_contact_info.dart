import 'package:flutter/material.dart';

class CheckoutContactInfo extends StatelessWidget {
  const CheckoutContactInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Column(
          children: [
            Text(
              "Checkout",
              style: TextStyle(fontSize: 18),
            ),
            Text(
              "1 of 3",
              style: TextStyle(fontSize: 12, color: Colors.grey),
            )
          ],
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Contact Info",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                  ),
                  Card(
                    elevation: 0,
                    color: Colors.grey[200],
                    child: ListTile(
                      title: Text(
                        "Full Name",
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                      subtitle: Text(
                        "Hanna Gouse",
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 0,
                    color: Colors.grey[200],
                    child: ListTile(
                      title: Text(
                        "Phone",
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                      subtitle: Text(
                        "+7 932 123-43-23",
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 0,
                    color: Colors.grey[200],
                    child: ListTile(
                      title: Text(
                        "Email",
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                      subtitle: Text(
                        "Hanna-g0@gmail.com",
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 20),
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: Colors.amber[300],
                      minimumSize: const Size.fromHeight(50)),
                  child: const Text('Show 25 items'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
