import 'package:flutter/material.dart';
import 'package:flutter_exercise_ulmo/config/constants.dart';
import 'package:flutter_exercise_ulmo/widgets/review_widget.dart';
import 'package:flutter_exercise_ulmo/widgets/search_widget.dart';

class ReviewPage extends StatelessWidget {
  const ReviewPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text("Reviews"),
        centerTitle: true,
        elevation: 0,
        actions: [
          TextButton(
              onPressed: () {},
              child: Text(
                "New Review",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              SearchWidget(),
              SizedBox(
                height: 10,
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return ReviewWidget(
                      star: dataReview[index]['star'],
                      name: dataReview[index]['name'],
                      time: dataReview[index]['time'],
                      comment: dataReview[index]['comment'],
                      attachment: dataReview[index]['attachment']);
                },
                itemCount: dataReview.length,
              )
            ],
          ),
        ),
      ),
    );
  }
}
