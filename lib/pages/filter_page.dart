import 'package:flutter/material.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  State<FilterPage> createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  double _currentSliderValue = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: Text("Filter"),
          leading: Icon(Icons.close),
          centerTitle: true,
          elevation: 0,
          actions: [
            TextButton(
                onPressed: () {},
                child: Text(
                  "Clear",
                  style: TextStyle(fontSize: 15, color: Colors.black),
                ))
          ],
        ),
        body: Stack(
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text("0"), Text("100")],
                  ),
                ),
                Slider(
                  value: _currentSliderValue,
                  max: 100,
                  divisions: 5,
                  label: _currentSliderValue.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentSliderValue = value;
                    });
                  },
                ),
                ListTile(
                  title: Text("Category"),
                  trailing: Text(
                    "furniture",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                ListTile(
                  title: Text("Product Type"),
                  trailing: Text(
                    "All",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                ListTile(
                  title: Text("Color"),
                  trailing: Text(
                    "All",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                ListTile(
                  title: Text("Size"),
                  trailing: Text(
                    "All",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                ListTile(
                  title: Text("Quality"),
                  trailing: Text(
                    "All",
                    style: TextStyle(color: Colors.grey),
                  ),
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: Colors.amber[300],
                      minimumSize: const Size.fromHeight(50)),
                  child: const Text('Show 25 items'),
                ),
              ),
            )
          ],
        ));
  }
}
