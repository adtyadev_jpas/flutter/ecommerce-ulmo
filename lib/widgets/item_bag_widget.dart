import 'package:flutter/material.dart';

class ItemBagWidget extends StatefulWidget {
  String price;
  String description;
  String url;

  ItemBagWidget(
      {Key? key,
      required this.price,
      required this.description,
      required this.url})
      : super(key: key);

  @override
  State<ItemBagWidget> createState() => _ItemBagWidgetState();
}

class _ItemBagWidgetState extends State<ItemBagWidget> {
  int itemCount = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      constraints: BoxConstraints(maxHeight: 130),
      //decoration: BoxDecoration(color: Colors.red),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: AssetImage(widget.url),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.price,
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    Text(
                      widget.description,
                      style: TextStyle(color: Colors.grey),
                    ),
                    ToggleButtons(
                      children: <Widget>[
                        Text("-"),
                        Text("$itemCount"),
                        Text("+"),
                      ],
                      borderRadius: BorderRadius.circular(5),
                      constraints: BoxConstraints(minWidth: 30, minHeight: 30),
                      onPressed: (int index) {
                        setState(() {
                          if (index == 0) {
                            (itemCount != 0) ? itemCount-- : itemCount;
                          } else if (index == 2) {
                            itemCount++;
                          }
                          // isSelected[index] = !isSelected[index];
                        });
                      },
                      isSelected: [false, false, false],
                    ),
                  ],
                ),
              )),
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.topRight,
              child: InkWell(
                  onTap: (){},
                  child: Icon(Icons.close_rounded)),
            ),
          )
        ],
      ),
    );
  }
}
