import 'package:flutter/material.dart';

class ReviewWidget extends StatelessWidget {
  int star;
  String name;
  String time;
  String comment;
  bool attachment;

  ReviewWidget(
      {Key? key,
      required this.star,
      required this.name,
      required this.time,
      required this.comment,
      required this.attachment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                  text: TextSpan(children: [
                for (int i = 0; i < star; i++)
                  WidgetSpan(
                      child: Icon(Icons.star, color: Colors.amberAccent)),
                for (int i = 0; i < 5 - star; i++)
                  WidgetSpan(
                      child: Icon(Icons.star_border_outlined,
                          color: Colors.amberAccent))
              ])),
              Text(time)
            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundImage: NetworkImage("https://i.pravatar.cc/300"),
                ),
              ),
              Text(
                name,
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
              )
            ],
          ),
          Text(
            comment,
            style: TextStyle(color: Colors.grey, fontSize: 15),
          ),
          attachment == true
              ? Container(
                  margin: EdgeInsets.only(top: 8),
                  constraints: BoxConstraints(
                    maxHeight: 50,
                  ),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Image.asset(
                          "assets/images/review1.png",
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Image.asset(
                          "assets/images/review2.png",
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Image.asset(
                          "assets/images/review3.png",
                        ),
                      )
                    ],
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
